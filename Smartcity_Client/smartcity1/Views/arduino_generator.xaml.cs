﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using smartcity1.Models;
using System.Collections.ObjectModel;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class arduino_generator : ContentPage
	{
        public ObservableCollection<string> Items1 { get; set; }
        public arduino_generator ()
		{
			InitializeComponent ();
            init();
            GetSensorList();
        }
        void init()
        {
            BackgroundColor = Constants.BackgroundColor;
            MyListView.SeparatorColor = Color.White;

        }

        private async void GetSensorList()
        {
            HttpResponseMessage response = await user.client.GetAsync(Constants.BaseUrl + "/smartcity/data.php");
            HttpContent content = response.Content;
            string mycontent = await content.ReadAsStringAsync();
            string[] sensors = mycontent.Split(',');
            indicator.IsVisible = false;
            //indicator.IsEnabled = false;
            indicator.IsRunning = false;
            Items1 = new ObservableCollection<string>(sensors);
            MyListView.ItemsSource = Items1;

        }
        protected override void OnAppearing()
        {
            root.IsEnabled = true;
        }

        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            root.IsEnabled = false;
            HttpResponseMessage response = await user.client.GetAsync(Constants.BaseUrl + "/smartcity/getUserId.php");
            HttpContent content = response.Content;
            string mycontent = await content.ReadAsStringAsync();


            indicator.IsVisible = true;
            //indicator.IsEnabled = false;
            indicator.IsRunning = true;
            if (e.Item == null)
                return;

            var values = new Dictionary<string, string>
            {
            { "sensor", e.Item.ToString() },
            { "userid", mycontent }
            };


            content = new FormUrlEncodedContent(values);
            response = await user.client.PostAsync(Constants.GeneratorURL + "/smartcity/arduino_generator.php", content);
            string responseString = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine(responseString);
            indicator.IsVisible = false;
            //indicator.IsEnabled = false;
            indicator.IsRunning = false;
            if (responseString.Equals("0"))
            {
                var result = await DisplayAlert("Code Generator", "Code Has been Generated! An e-mail has been sent to your address.", "Ok", "Cancel");
                if (result == true)
                    await Navigation.PopToRootAsync();
            }
            else
            {
                await DisplayAlert("Code Generator", "Something went wrong.. Please try again", "Ok");
                root.IsEnabled = true;
            }
           

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }



    }
}