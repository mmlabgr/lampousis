﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Editor : ContentPage
    {
        static string _globalString = "";

        public static string GlobalString
        {
            get
            {
                return _globalString;
            }
            set
            {
                _globalString = value;
            }
        }

        public Editor(string SelectedSensor)
        {
            InitializeComponent();
            init();
            Editor.GlobalString = SelectedSensor;

        }

        void init()
        {
            BackgroundColor = Constants.BackgroundColor;
            Editor_Page.TextColor = Constants.MainTextColor;
            btn1.TextColor = Color.Black;
            btn2.TextColor = Color.Black;


        }

        async void OnButtonClicked1(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new EditPayload(Editor.GlobalString));
        }



        async void OnButtonClicked2(object sender, EventArgs args)
        {
            string str = Editor.GlobalString;
           
            str = str.Replace(" ", String.Empty);
            System.Diagnostics.Debug.WriteLine(str);
            string alert = "Warning! All data will be lost. If you want to remove device: " + str + " press Ok";
            var result = await DisplayAlert("Remove Device", alert, "Ok", "Cancel");
            if (result == true)
            {
                var values = new Dictionary<string, string>
                {
                    { "device", str  }
                 };
          
                var content = new FormUrlEncodedContent(values);
                var response = await user.client.PostAsync(Constants.BaseUrl + "/smartcity/removedev.php", content);
                string responseString = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(responseString);

                if (responseString == "success")
                {
                    await Navigation.PopToRootAsync();
                }
            }
        }
    }
}