﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Xamarin.Forms;

namespace smartcity1.Models
{
    
    public class Constants
    {
        public static string GeneratorURL
        {
            get;
            set;
        }
        public static bool IsDev = true;
        public static Color BackgroundColor = Color.FromRgb(58, 153, 215);
        public static Color MainTextColor = Color.White;
        public static string BaseUrl = "http://baseURL";
        public static int LoginIconHeight = 120;
    } 

    
}
