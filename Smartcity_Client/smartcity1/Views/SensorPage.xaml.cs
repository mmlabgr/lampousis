﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using smartcity1.Models;
using System.Collections.ObjectModel;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SensorPage : ContentPage
	{
        public ObservableCollection<string> Items { get; set; }
        public SensorPage ()
		{
			InitializeComponent();
            init();
            GetSensorList();
		}

        void init()
        {
            BackgroundColor = Constants.BackgroundColor;
            MyListView.SeparatorColor = Color.White;
           
        }

        protected override void OnAppearing()
        {
            root.IsEnabled = true;
        }
        private async void GetSensorList()
        {
            
            HttpResponseMessage response = await user.client.GetAsync(Constants.BaseUrl + "/smartcity/data.php");
            HttpContent content = response.Content;
            indicator.IsVisible = false ;
            //indicator.IsEnabled = false;
            indicator.IsRunning = false;
          
            string mycontent = await content.ReadAsStringAsync();
            string[] sensors = mycontent.Split(',');

            Items = new ObservableCollection<string>(sensors);
            MyListView.ItemsSource = Items;
            root.IsEnabled = true;
        }
        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            root.IsEnabled = false;

            string action = await DisplayActionSheet("Select View", "Cancel", null, "Data"/*, "Graph"*/);
            if(action.Equals("Data"))
            {
                await Navigation.PushAsync(new Data(e.Item.ToString()));
            }
            if(action.Equals("Graph"))
            {
                await Navigation.PushAsync(new DataGraph(e.Item.ToString()));
            }

            

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
       


    }
}