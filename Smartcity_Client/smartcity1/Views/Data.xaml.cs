﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Data : ContentPage
	{
        List<ListViewItem> Items1;
        public ObservableCollection<string> Items { get; set; }
        public Data(string SelectedSensor)
        {
            InitializeComponent();
            init();
            GetSensorData(SelectedSensor);
        }

        void init()
        {
            BackgroundColor = Constants.BackgroundColor;
            MyListView.SeparatorColor = Color.White;
            

        }

   
        public async void GetSensorData(string SelectedSensor)
        {
            
            String[] temp_data;
            var Values = new Dictionary<string, string>
            {
            { "sensor_name", SelectedSensor }
            };

            var content = new FormUrlEncodedContent(Values);
            var response = await user.client.PostAsync(Constants.BaseUrl + "/smartcity/sensor_data_v2.php", content);
            string responseString = await response.Content.ReadAsStringAsync();

            if ( responseString.Count() > 0)
            {
                System.Diagnostics.Debug.WriteLine(responseString);
                string[] data = responseString.Split(',');
                String devid = data[0].Split(';')[0];
                devid = devid.Split(':')[1];
                Items1 = new List<ListViewItem>();

                for (int i = 0; i < data.Length; i++)
                {
                    temp_data = data[i].Split(';');
                    for (int y = 1; y < temp_data.Length; y++)
                    {
                        temp_data[y] = temp_data[y] + "\n";
                    }

                    Items1.Add(new ListViewItem
                    {
                        Name = temp_data[0],
                        Values = string.Join("", temp_data, 1, temp_data.Length - 2),
                        TimeStamp = temp_data[temp_data.Length - 1].Split(':')[1]

                    });
                }
                Title.BindingContext = devid;
                MyListView.ItemsSource = Items1;
            }
            else
            {
                await DisplayAlert("Error", "Please try again.", "OK");
                await Navigation.PopToRootAsync();
            }
            
        }
        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            //await DisplayAlert("Item Tapped", "An item was tapped.", "OK");

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
    }
}