import sys
import binascii
import time
import os
import errno


nwskey = sys.argv[1]
appskey = sys.argv[2]
devaddr = sys.argv[3]
device = sys.argv[4]
sensor = sys.argv[5]
columns = sys.argv[6]
payload_format = sys.argv[7]
mynwskey =""
myappskey = ""
ardu_code = ""



#Converts HEX keys FFDD... into C- Style 0xFF, 0xDD, ...
for x in range(0,29,2):
	mynwskey = mynwskey +"0x"+ nwskey[x]+nwskey[x+1]+", "

mynwskey = mynwskey +"0x"+ nwskey[30]+nwskey[31]

for x in range(0,29,2):
	myappskey = myappskey +"0x"+ appskey[x]+appskey[x+1]+", "

myappskey = myappskey +"0x"+ appskey[30]+appskey[31]

#data_folder = os.path.join("E:\\","xampp","htdocs","smartcity","arduino","code_templates")
#file_to_open = os.path.join(data_folder,"1.txt")


ardu_code = """


//Arduino Uno + Dragino 1.4 + DHT11 PIN 7

#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <dht11.h>
#include <string.h>
#define DHT11PIN 7
dht11 DHT11;

static const PROGMEM u1_t NWKSKEY[16] = { """ +mynwskey+ """ };

static const u1_t PROGMEM APPSKEY[16] = { """ +myappskey+ """ };

static const u4_t DEVADDR = 0x""" +devaddr+ """;

void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }


static uint8_t mydata[] = "";
static osjob_t initjob,sendjob,blinkjob;

const unsigned TX_INTERVAL = 45;


// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = 10,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 9,
    .dio = {2, 6, 7},
};



void do_send(osjob_t* j){
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println("OP_TXRXPEND, not sending");
    } else {
        //Get DH11 data
        int chk = DHT11.read(DHT11PIN);
       
        uint16_t temperature = DHT11.temperature*100 ;
        uint16_t humidity = DHT11.humidity*100 ;
        byte payload[4];
       
        payload[0]=highByte(temperature);
        payload[1]=lowByte(temperature);
        payload[2]=highByte(humidity);
        payload[3]=lowByte(humidity);

 
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, payload, sizeof(payload), 0);
        Serial.println("Packet queued");
        Serial.println(LMIC.freq);
    }
    // Next TX is scheduled after TX_COMPLETE event.
}



void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    Serial.println(ev);
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println("EV_SCAN_TIMEOUT");
            break;
        case EV_BEACON_FOUND:
            Serial.println("EV_BEACON_FOUND");
            break;
        case EV_BEACON_MISSED:
            Serial.println("EV_BEACON_MISSED");
            break;
        case EV_BEACON_TRACKED:
            Serial.println("EV_BEACON_TRACKED");
            break;
        case EV_JOINING:
            Serial.println("EV_JOINING");
            break;
        case EV_JOINED:
            Serial.println("EV_JOINED");
            break;
        case EV_RFU1:
            Serial.println("EV_RFU1");
            break;
        case EV_JOIN_FAILED:
            Serial.println("EV_JOIN_FAILED");
            break;
        case EV_REJOIN_FAILED:
            Serial.println("EV_REJOIN_FAILED");
            break;
        case EV_TXCOMPLETE:
            Serial.println("EV_TXCOMPLETE (includes waiting for RX windows)");
            if(LMIC.dataLen) {
                // data received in rx slot after tx
                Serial.print("Data Received: ");
                Serial.write(LMIC.frame+LMIC.dataBeg, LMIC.dataLen);
                Serial.println();
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            Serial.println("EV_LOST_TSYNC");
            break;
        case EV_RESET:
            Serial.println("EV_RESET");
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println("EV_RXCOMPLETE");
            break;
        case EV_LINK_DEAD:
            Serial.println("EV_LINK_DEAD");
            break;
        case EV_LINK_ALIVE:
            Serial.println("EV_LINK_ALIVE");
            break;
         default:
            Serial.println("Unknown event");
            break;
    }
}

void setup() {
    Serial.begin(9600);
    Serial.println("Starting");
    
    #ifdef VCC_ENABLE
    pinMode(VCC_ENABLE, OUTPUT);
    digitalWrite(VCC_ENABLE, HIGH);
    delay(1000);
    #endif

    os_init();
    LMIC_reset();
  
    #ifdef PROGMEM
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif
    
 
    LMIC_setLinkCheckMode(0);
    LMIC.dn2Dr = DR_SF12;
    LMIC_setDrTxpow(DR_SF7,14);
    do_send(&sendjob);
}

void loop() {
    os_runloop_once();
}


"""



data_folder = os.path.join("E:\\","xampp","htdocs","smartcity","arduino",devaddr)
file_to_open = os.path.join(data_folder, devaddr+".ino")
build_folder = os.path.join("E:\\","xampp","htdocs","smartcity","arduino","builds",devaddr)
os.makedirs(data_folder)
os.makedirs(build_folder)
f = open(file_to_open,"w+")
f.write(ardu_code)
f.close()



