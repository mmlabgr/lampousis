 param (
    [string]$toaddress = 0, 
    [string]$body = 0,    
    [string]$attachment = 0
)


 $fromaddress = "email@domain.com"
 $Subject = "SmartCity Device Code"
 $smtpserver = "smtp.gmail.com"

 $message = new-object System.Net.Mail.MailMessage
 $message.From = $fromaddress
 $message.To.Add($toaddress)
 $message.Subject = $Subject
 $attach = new-object Net.Mail.Attachment($attachment)
 $message.Attachments.Add($attach)
 $message.body = $body

 $SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer, 587)
 $SMTPClient.EnableSsl = $true
 $SMTPClient.Credentials = New-Object System.Net.NetworkCredential("email@domain.com", "password");
 $SMTPClient.Send($message)