﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using SkiaSharp;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Text.RegularExpressions;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DataGraph : ContentPage
	{
        public int index;
        public bool monthSelected;
        public bool dataSelected;
        List<ListViewItem> Items1;
        List<ListViewItem> Items3;
        public ObservableCollection<string> Items { get; set; }
        public ObservableCollection<ListViewItem1> Items2 { get; set; }
        public DataGraph (string SelectedSensor)
		{
            InitializeComponent();
            //ListView.IsVisible = false;
            //webView.IsVisible = false;
            GetSensorData(SelectedSensor);
           // init();
        }

        public void init()
        {
            Picker1.MaximumDate = CalculateMaximumDate();
            Picker1.MinimumDate = CalculateMinimumDate();
        }


        public int MaxValue(int column)
        {
            int maxValue = 0;
            foreach(var data in Items1)
            {
                string splt = data.Values.Split('\n')[column];
                int value = 0;
                int.TryParse(splt.Split(':')[1], out value);
                if(value > maxValue)
                {
                    maxValue = value;
                }
            }
            return maxValue;
        }

        public int MinValue(int column)
        {
            int minValue = 9999999;
            foreach (var data in Items1)
            {
                string splt = data.Values.Split('\n')[column];
                int value = 0;
                int.TryParse(splt.Split(':')[1] , out value);
                if (value < minValue)
                {
                    minValue = value;
                }
            }
            return minValue;
        }

        public DateTime CalculateMaximumDate()
        {
            DateTime Max = DateTime.ParseExact("0000-00-00", "yyyy-MM-dd", null);
            foreach (var date in Items1)
            {
                string dateyymmdd = date.TimeStamp.Split(' ')[0];
                DateTime temp = DateTime.ParseExact(dateyymmdd, "yyyy-MM-dd", null);
                if(temp > Max)
                {
                    Max = temp;
                }
            }
            return Max;
        }

        public DateTime CalculateMinimumDate()
        {
            DateTime Min = DateTime.Now.AddYears(1);
            foreach (var date in Items1)
            {
                string dateyymmdd = date.TimeStamp.Split(' ')[0];
                DateTime temp = DateTime.ParseExact(dateyymmdd, "yyyy-MM-dd", null);
                if (temp < Min)
                {
                    Min = temp;
                }
            }
            return Min;
        }


        public async void GetSensorData(string SelectedSensor)
        {

            String[] temp_data;
            var Values = new Dictionary<string, string>
            {
            { "sensor_name", SelectedSensor }
            };

            var content = new FormUrlEncodedContent(Values);
            var response = await user.client.PostAsync(Constants.BaseUrl + "/smartcity/sensor_data_v2.php", content);
            string responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Count() > 0)
            {
                System.Diagnostics.Debug.WriteLine(responseString);
                string[] data = responseString.Split(',');
                String devid = data[0].Split(';')[0];
                devid = devid.Split(':')[1];
                Items1 = new List<ListViewItem>();

                for (int i = 0; i < data.Length; i++)
                {
                    temp_data = data[i].Split(';');
                    for (int y = 1; y < temp_data.Length; y++)
                    {
                        temp_data[y] = temp_data[y] + "\n";
                    }

                    Items1.Add(new ListViewItem
                    {
                        Name = temp_data[0],
                        Values = string.Join("", temp_data, 1, temp_data.Length - 2),
                        TimeStamp = temp_data[temp_data.Length - 1].Split(':')[1]

                    });
                }
            }
            else
            {
                await DisplayAlert("Error", "Please try again.", "OK");
                await Navigation.PopToRootAsync();
            }
        }

        private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            Items2 = new ObservableCollection<ListViewItem1>();
            Items3 = new List<ListViewItem>();
            foreach (var date in Items1)
            {
                string dateyymmdd = date.TimeStamp.Split(' ')[0];
                DateTime temp = DateTime.ParseExact(dateyymmdd, "yyyy-MM-dd", null);
                if(temp.Year == Picker1.Date.Year && temp.Month == Picker1.Date.Month)
                {
                    Items3.Add(date);
                }
            }

            
            for (int i = 1; i <= DateTime.DaysInMonth(Picker1.Date.Year, Picker1.Date.Month); i++)
            {
                int avg = 0;
                int j = 0;
                foreach (var data in Items3)
                {
                    
                    string dateyymmdd = data.TimeStamp.Split(' ')[0];
                    DateTime temp = DateTime.ParseExact(dateyymmdd, "yyyy-MM-dd", null);
                    if (temp.Day == i)
                    {
                        string splt = Items3[i].Values.Split('\n')[index];
                        int value = 0;
                        int.TryParse(splt.Split(':')[1], out value);
                        avg += value;
                        j++;
                    }
                    
                }
                if(j > 0)
                {
                    avg = (int)((double)(avg / j));
                }
                else
                {
                    avg = 0;
                }
                int datavalue = (int)(((double)avg / MaxValue(index)));
                //int bottomMargin = (datavalue * (int)GetHeight.Height);
                var lst = new ListViewItem1()
                {
                    day = i.ToString(),
                    //margin = "0,0,0," + bottomMargin.ToString(),
                    data = datavalue.ToString()
                };
                Items2.Add(lst);
            }
            //ListView.BindingContext = Items2;
            monthSelected = true;

            if (dataSelected && monthSelected)
            {
                //ListView.IsVisible = true;m
                string UrlPart1 = "http://charts.hohli.com/embed.html?created=1570888196343#w=320&h=320&d={\"containerId\":\"chart\",\"dataTable\":{\"cols\":[{\"label\":\"A\",\"type\":\"string\",\"p\":{}},{\"label\":\"B\",\"type\":\"number\"}],";
                string UrlPart3 = "\"options\":{\"hAxis\":{\"useFormatFromData\":true,\"viewWindow\":null,\"minValue\":null,\"maxValue\":null,\"viewWindowMode\":null},\"legacyScatterChartLabels\":true,\"vAxes\":[{\"useFormatFromData\":true,\"viewWindow\":{\"max\":null,\"min\":null},\"minValue\":null,\"maxValue\":null},{\"useFormatFromData\":true,\"viewWindow\":{\"max\":null,\"min\":null},\"minValue\":null,\"maxValue\":null}],\"curveType\":\"\",\"booleanRole\":\"certainty\",\"lineWidth\":2,\"legend\":\"right\",\"width\":640,\"height\":480},\"state\":{},\"view\":{\"columns\":null,\"rows\":null},\"isDefaultVisualization\":false,\"chartType\":\"LineChart\"}";
                string UrlPart2Start = "\"rows\":[";
                string UrlPart2End = "]},";
                string values1 = string.Empty;
                string values2 = string.Empty;
                string values3 = string.Empty;
                string values4 = string.Empty;
                string values5 = string.Empty;


                foreach (var item in Items2)
                {
                    values1 = "{\"c\":[{\"v\":\"";
                    values2 = item.day;
                    values3 = "\"},{\"v\":";
                    values4 = item.data;
                    values5 = "}]},";
                }

                string UrlPart2Middle = values1 + values2 + values3 + values4 + values5;
              

                string UrlFull = UrlPart1 + UrlPart2Start + UrlPart2Middle + UrlPart2End + UrlPart3;
                UrlFull = UrlFull.Replace("\"", "%22");
                
                //var uri = new UriBuilder();
                //uri.Path = UrlFull;

                webView.Source = UrlFull;





            }
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            string[] colmns;
            List<string> names = new List<string>();
            colmns = Items1[0].Values.Split('\n');
            foreach(var clm in colmns)
            {
                names.Add(clm.Split(':')[0]);
            }
            colmns = names.ToArray();

            string action = await DisplayActionSheet("Select Data", "Cancel", null, colmns);
            index = names.IndexOf(action);
            //initGraph(MinValue(names.IndexOf(action)), MaxValue(names.IndexOf(action)));
            dataSelected = true;

            if (dataSelected && monthSelected)
            {
                //ListView.IsVisible = true;
                //webView.IsVisible = true;
                
            }

        }

        //public void initGraph(int min, int max)
        //{

        //    int range = (int)((max - min)/(float)5);
        //    lbl1.Text = max.ToString();
        //    lbl2.Text = (min + 4*range).ToString();
        //    lbl3.Text = (min + 3*range).ToString();
        //    lbl4.Text = (min + 2*range).ToString();
        //    lbl5.Text = (min + range).ToString();
        //    lbl6.Text = min.ToString();

        //}
    }
}