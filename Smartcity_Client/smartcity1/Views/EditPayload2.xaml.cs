﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditPayload2 : ContentPage
	{

        static string _globalString = "";

        public static string GlobalString
        {
            get
            {
                return _globalString;
            }
            set
            {
                _globalString = value;
            }
        }
        public EditPayload2 (string SelectedSensor)
		{
			InitializeComponent ();
            EditPayload.GlobalString = SelectedSensor;
            Init();
        }

        void Init()
        {
            BackgroundColor = Constants.BackgroundColor;
            Lbl_Column_Name_1.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_1.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_2.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_2.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_3.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_3.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_4.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_4.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_5.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_5.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_6.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_6.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_7.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_7.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_8.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_8.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_9.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_9.TextColor = Constants.MainTextColor;
            Lbl_Column_Name_10.TextColor = Constants.MainTextColor;
            Lbl_Format_Size_10.TextColor = Constants.MainTextColor;


            Entry_Column_Name_1.Completed += (s, e) => Entry_Format_Size_1.Focus();
            Entry_Format_Size_1.Completed += (s, e) => Entry_Column_Name_2.Focus();
            Entry_Column_Name_2.Completed += (s, e) => Entry_Format_Size_2.Focus();
            Entry_Format_Size_2.Completed += (s, e) => Entry_Column_Name_3.Focus();
            Entry_Column_Name_3.Completed += (s, e) => Entry_Format_Size_3.Focus();
            Entry_Format_Size_3.Completed += (s, e) => Entry_Column_Name_4.Focus();
            Entry_Column_Name_4.Completed += (s, e) => Entry_Format_Size_4.Focus();
            Entry_Format_Size_4.Completed += (s, e) => Entry_Column_Name_5.Focus();
            Entry_Column_Name_5.Completed += (s, e) => Entry_Format_Size_5.Focus();
            Entry_Format_Size_5.Completed += (s, e) => Entry_Column_Name_6.Focus();
            Entry_Column_Name_6.Completed += (s, e) => Entry_Format_Size_6.Focus();
            Entry_Format_Size_6.Completed += (s, e) => Entry_Column_Name_7.Focus();
            Entry_Column_Name_7.Completed += (s, e) => Entry_Format_Size_7.Focus();
            Entry_Format_Size_7.Completed += (s, e) => Entry_Column_Name_8.Focus();
            Entry_Column_Name_8.Completed += (s, e) => Entry_Format_Size_8.Focus();
            Entry_Format_Size_8.Completed += (s, e) => Entry_Column_Name_9.Focus();
            Entry_Column_Name_9.Completed += (s, e) => Entry_Format_Size_9.Focus();
            Entry_Format_Size_9.Completed += (s, e) => Entry_Column_Name_10.Focus();
            Entry_Column_Name_10.Completed += (s, e) => Entry_Format_Size_10.Focus();
            

       

        }

        async void UpdateDevicePayload(object sender, EventArgs e)
        {
            string str = EditPayload2.GlobalString;
            str = str.Replace(" ", String.Empty);
            string column_names = Entry_Column_Name_1.Text + "," + Entry_Column_Name_2.Text + "," + Entry_Column_Name_3.Text + "," + Entry_Column_Name_4.Text + "," + Entry_Column_Name_5.Text + "," + Entry_Column_Name_6.Text + "," + Entry_Column_Name_7.Text + "," + Entry_Column_Name_8.Text + "," + Entry_Column_Name_9.Text + "," + Entry_Column_Name_10.Text;
            string format_size = Entry_Format_Size_1.Text + "," + Entry_Format_Size_2.Text + "," + Entry_Format_Size_3.Text + "," + Entry_Format_Size_4.Text + "," + Entry_Format_Size_5.Text + "," + Entry_Format_Size_6.Text + "," + Entry_Format_Size_7.Text + "," + Entry_Format_Size_8.Text + "," + Entry_Format_Size_9.Text + "," + Entry_Format_Size_10.Text;


            var values = new Dictionary<string, string>
            {
            { "column_names", column_names },
            { "format", format_size },
            { "sensor", str }
            };

            System.Diagnostics.Debug.WriteLine(str);

            var content = new FormUrlEncodedContent(values);
            var response = await user.client.PostAsync(Constants.BaseUrl + "/smartcity/edit_payload.php", content);
            string responseString = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine(responseString);



        }
    }
}