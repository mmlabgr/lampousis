﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserPage : ContentPage
	{
		public UserPage ()
		{
			InitializeComponent ();
            init();
        }

        void init()
        {
            
            BackgroundColor = Constants.BackgroundColor;
            User_Page.TextColor = Constants.MainTextColor;
            btn1.TextColor = Color.Black;
            btn2.TextColor = Color.Black;
        }

        async void OnButtonClicked1(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new SensorPage());
        }

        async void OnButtonClicked2(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new devices());
        }
    }
}