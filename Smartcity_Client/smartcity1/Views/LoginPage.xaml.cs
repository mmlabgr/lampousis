﻿using smartcity1.Models;
using smartcity1.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        
        public LoginPage ()
		{
			InitializeComponent ();
            GetGeneratorURL();
            Init();
           
        }

        public async void GetGeneratorURL()
        {
            HttpResponseMessage response = await user.client.GetAsync(Constants.BaseUrl + "/smartcity/GetGeneratorURL.php");
            HttpContent content = response.Content;
            string mycontent = await content.ReadAsStringAsync();
            Constants.GeneratorURL = mycontent;
        }

        protected override void OnAppearing()
        {
            root.IsEnabled = true;
        }

        void Init()
        {
            BackgroundColor = Constants.BackgroundColor;
            Lbl_Username.TextColor = Constants.MainTextColor;
            Lbl_Password.TextColor = Constants.MainTextColor;
            ActivitySpinner.IsVisible = false;
            LoginIcon.HeightRequest = Constants.LoginIconHeight;
            

            Entry_Username.Completed += (s, e) => Entry_Password.Focus();
            Entry_Password.Completed += (s, e) => SignInProcedure(s,e);
        }
      
        async void SignInProcedure(object sender, EventArgs e)
        {
            root.IsEnabled = false;
            user user = new user(Entry_Username.Text, Entry_Password.Text);
            System.Diagnostics.Debug.WriteLine(user.CheckInformation());
            string response = await user.CheckInformation();
            if (response.Equals("success"))
            {
                 Application.Current.MainPage =  new NavigationPage(new UserPage());
            }
            else
            {
                await DisplayAlert("Login", "Login Error, Please Check Username and Password.", "Ok");
                root.IsEnabled = true;
            }
               
        }

        async void SignUpProcedure(object sender, EventArgs e)
        {
                await Navigation.PushModalAsync(new Registration());
        }

    }
}