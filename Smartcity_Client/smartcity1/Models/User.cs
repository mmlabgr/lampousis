﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

namespace smartcity1.Models
{
    public class user
    {
        
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password {get; set; }
        public user() { }
        public user(string Username, string Password)
        {
            this.Username = Username;
            this.Password = Password;
        }


        public static HttpClient client = new HttpClient();


        public async Task<string> CheckInformation()
        {
            var values = new Dictionary<string, string>
            {
            { "username", this.Username },
            { "password", this.Password }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync(Constants.BaseUrl+"/smartcity/login.php", content);
            var responseString = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine(responseString);
            return responseString;

        }
       

      
    }
}
