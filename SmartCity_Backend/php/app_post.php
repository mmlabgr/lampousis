<?php
   ini_set('display_errors', 1);
   include("Config.php");
   session_start();
   
     if($_SERVER["REQUEST_METHOD"] == "POST") {
	 
	  //decodes the POSTED json object to a PHP array
	  $data = json_decode( file_get_contents( 'php://input' ), true );
	 

	 $table_values = "";
	 $payload_format =""; 
	
	 
	  //constructs the appropriate sql querry 
	  while( $key_data = current($data) )
	  {
		  if(strcmp(key($data),"dev_id") !== 0)
		  {
			  $table_values .= key($data);
			  $type = explode(':',$key_data);
			  
			  $payload_format.= $key_data;
			  $payload_format.= '/';
			  
			  switch ( $type[0] )
			  {
				case "bits":
			    $table_values .= " bit";
				
				break;
				
				case "int":
				$table_values .= " int";
				
				break;
				
				case "float":
				$table_values .= " float";
				
				break;
				
				case "string":
				$table_values .= " varchar(10)";
				
				break;

				  
			  }
			   
			   
			 
			   $table_values .= ',';
			  
			  
		  
		  
		  }
		  next($data);
	  }
	  
	  $trimed_names = rtrim($table_values,", ");
	  echo $trimed_names ;
	  
	  //payload format to be inserted to sensors table.
	  $trimed_payload = rtrim($payload_format,"/ ");
	  echo $trimed_payload;
	  
	  
      //Create table into database 
      $sql = "CREATE TABLE ".$data["dev_id"]." (".$trimed_names.")";
	  $result = mysqli_query($db,$sql);
	  
	 }
?>

