﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class devmgr : ContentPage
    {
        public devmgr()
        {
            InitializeComponent();
            Init();

        }

        void Init()
        {
            BackgroundColor = Constants.BackgroundColor;
            Lbl_sensor_name.TextColor = Constants.MainTextColor;
            Lbl_description.TextColor = Constants.MainTextColor;
            Lbl_Column_Names.TextColor = Constants.MainTextColor;
            Lbl_Format_Size.TextColor = Constants.MainTextColor;
            Lbl_device_pick.TextColor = Constants.MainTextColor;
            Lbl_sensor_pick.TextColor = Constants.MainTextColor;
            indicator.IsVisible = false;
            //indicator.IsEnabled = false;
            indicator.IsRunning = false;

            /*Lbl_fCntUp.TextColor = Constants.MainTextColor;
            Lbl_fCntDown.TextColor = Constants.MainTextColor;
            Lbl_latitude.TextColor = Constants.MainTextColor;
            Lbl_longitude.TextColor = Constants.MainTextColor;
            Lbl_altitude.TextColor = Constants.MainTextColor;*/





            Entry_sensor_name.Completed += (s, e) => Entry_description.Focus();
            Device_pick.SelectedIndexChanged += this.myPickerSelectedIndexChanged;
            /*Entry_description.Completed += (s, e) => RegisterDeviceProcedure(s, e);
            Entry_fCntUp.Completed += (s, e) => Entry_fCntDown.Focus();
            Entry_fCntDown.Completed += (s, e) => Entry_latitude.Focus();
            Entry_latitude.Completed += (s, e) => Entry_longitude.Focus();
            Entry_longitude.Completed += (s, e) => Entry_altitude.Focus();
            Entry_altitude.Completed += (s, e) => RegisterDeviceProcedure(s, e);*/
        }

        public void myPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            
            string selectedValue = Device_pick.SelectedItem.ToString();
            if(selectedValue.Equals("custom"))
            {
                Lbl_Column_Names.IsVisible = true;
                Entry_Column_Names.IsVisible = true;
                Lbl_Format_Size.IsVisible = true;
                Entry_Format_Size.IsVisible = true;
            }else
            {
                Lbl_Column_Names.IsVisible = false;
                Entry_Column_Names.IsVisible = false;
                Lbl_Format_Size.IsVisible = false;
                Entry_Format_Size.IsVisible = false;

            }
        }

        async void RegisterDeviceProcedure(object sender, EventArgs e)
        {
            indicator.IsVisible = true;
            //indicator.IsEnabled = false;
            indicator.IsRunning = true;

            string device = Device_pick.SelectedItem.ToString();
            string sensor = Sensor_pick.SelectedItem.ToString();

     
            var values = new Dictionary<string, string>
            
            {
            { "sensor_name", Entry_sensor_name.Text },
            { "description", Entry_description.Text },
            { "fCntUp", "0"},
            { "fCntDown", "0" },
            { "latitude", "0" },
            { "longitude","0" },
            { "altitude", "0" },
            { "sensor", sensor },
            { "device", device },
            { "columns", Entry_Column_Names.Text },
            { "payload", Entry_Format_Size.Text }
            };

            var content = new FormUrlEncodedContent(values);
            System.Diagnostics.Debug.WriteLine(values);
            System.Diagnostics.Debug.WriteLine(content);
            var response = await user.client.PostAsync(Constants.BaseUrl + "/smartcity/devmgr_abp.php", content);
            string responseString = await response.Content.ReadAsStringAsync();

            System.Diagnostics.Debug.WriteLine(responseString);
            indicator.IsVisible = false;
            //indicator.IsEnabled = false;
            indicator.IsRunning = false;


            if (responseString.Equals("0"))
            {
                var result = await DisplayAlert("New Device", "New Device Has been Successfully Registered", "Ok", "Cancel");
                if (result == true)
                    Navigation.PopToRootAsync();

            }
            else
                await DisplayAlert("Registration", "There was an error, please try again", "Ok");
        }
    }
    
}