<?php
   ini_set('display_errors', 1);
   include("Config.php");
   session_start();
   
     if($_SERVER["REQUEST_METHOD"] == "POST") {
	 
	  //decodes the POSTED json object to a PHP array
	  $data = json_decode( file_get_contents( 'php://input' ), true );
	  
	  //Decodes the base64 raw payload from ttn
	  $payload = base64_decode($data["payload_raw"]);
	  
	  //unpacks the payload into HEX values into $value_hex[1]
	  $value_hex = unpack("H*", $payload);
	  $pad = strlen($value_hex[1])*4;
	  //converts the Hex value to bin
	  $value_bin = str_pad(base_convert($value_hex[1],16,2),$pad,'0',STR_PAD_LEFT);
	  
	  //Gets the payload format from the database
	  $sql = "SELECT payload_format FROM sensors WHERE dev_id = '".$data["dev_id"]."'" ;
	  $result = mysqli_query($db,$sql);
	  
	  while($row = mysqli_fetch_array($result))
	  $payload_format = $row[0];
  
      $payload_array = explode(",",$payload_format);
	  $payload_args = "";
	  
	  //Converts the values to the appropriate format for insetion
	  $pos = 0;
	  for($x=0;$x < count($payload_array);$x++)
	  {
		
		if($x!=0)
		{
        $payload_args.= ",'";
  		}	
		
		$payload_bytes = explode(":",$payload_array[$x]);
		
		switch ($payload_bytes[0])
		{
			case "bits":
			    $payload_args.= substr($value_bin,$pos,  ($payload_bytes[1]));
				break;
			case "uint":
			    $payload_args.= bindec(substr($value_bin,$pos,  ($payload_bytes[1])));
				break;
			
			case "int":
			    $payload_args.= (int)bindec(substr($value_bin,$pos,  ($payload_bytes[1])));
				break;
				
			case "float16":
			    $payload_args.=  bindec(substr($value_bin,$pos,  ($payload_bytes[1])))/100;
				break;
				
			case "float32":
			    $payload_args.=  (float)bindec(substr($value_bin,$pos,  ($payload_bytes[1])));
				break;
				
			case "string":
			    $payload_args.=  (string)bindec(substr($value_bin,$pos,  ($payload_bytes[1])));
				break;
				
			case "gps":
				break;	
		}
		
		$pos+= $payload_bytes[1];
		
		
		
	    if($x != count($payload_array)-1)
		{
			$payload_args.= "'";
		}
	  }
	  echo $payload_args; 
	  //gets the column names for the selected db table and puts it into a array
	  $column_names = array();
	  
	  $sql = " SHOW COLUMNS FROM  ".$data["dev_id"]." ";
	  $result = mysqli_query($db,$sql);
	  
	  while($row = mysqli_fetch_array($result))
      $column_names[] = $row['Field'];
	  
	  $joined_columns = join(",",$column_names);
	  
      //insert into database 
      $sql = "INSERT INTO ".$data["dev_id"]." (".$joined_columns.") VALUES ('".$data["dev_id"]."','".$payload_args."','".$data["metadata"]["time"]."')";
	  $result = mysqli_query($db,$sql);
	 }
?>

