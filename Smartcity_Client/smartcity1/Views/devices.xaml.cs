﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class devices : ContentPage
    {
        public devices ()
        {
            InitializeComponent();
            init();
        }

       
        void init()
        {
            BackgroundColor = Constants.BackgroundColor;
            Devices_Page.TextColor = Constants.MainTextColor;
            btn1.TextColor = Color.Black;
            btn2.TextColor = Color.Black;

        }

        async void OnButtonClicked1(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new EditDev());
        }

        async void OnButtonClicked2(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new devmgr());
        }

        async void OnButtonClicked3(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new arduino_generator());
        }
    }
}