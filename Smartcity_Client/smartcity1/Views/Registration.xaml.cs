﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Registration : ContentPage
	{
		public  Registration ()
		{
			InitializeComponent ();
            Init();
        }

        void Init()
        {
            BackgroundColor = Constants.BackgroundColor;
            Lbl_Username.TextColor = Constants.MainTextColor;
            Lbl_Password.TextColor = Constants.MainTextColor;
            Lbl_First_Name.TextColor = Constants.MainTextColor;
            Lbl_Last_Name.TextColor = Constants.MainTextColor;
            Lbl_Email.TextColor = Constants.MainTextColor;
            Lbl_PasswordR.TextColor = Constants.MainTextColor;




            Entry_First_Name.Completed += (s, e) => Entry_Last_Name.Focus();
            Entry_Last_Name.Completed += (s, e) => Entry_Username.Focus();
            Entry_Username.Completed += (s, e) => Entry_Email.Focus();
            Entry_Email.Completed += (s, e) => Entry_Password.Focus();
            Entry_Password.Completed += (s, e) => Entry_PasswordR.Focus();
            Entry_PasswordR.Completed += (s, e) => SingUpProcedure(s, e);
        }

        async void SingUpProcedure(object sender, EventArgs e)
        {


            var values = new Dictionary<string, string>
            {
            { "username", Entry_Username.Text },
            { "password", Entry_Password.Text },
            { "email", Entry_Email.Text },
            { "first_name", Entry_First_Name.Text},
            {"last_name", Entry_Last_Name.Text }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await user.client.PostAsync(Constants.BaseUrl + "/smartcity/register.php", content);
            string responseString = await response.Content.ReadAsStringAsync();
            
            if (responseString.Equals("success"))
            {
                var result = await DisplayAlert("Registration", "Registration Completed!", "Ok","Cancel");
                if (result==true)
                await Navigation.PushModalAsync(new LoginPage());

            }
            else
                await DisplayAlert("Registration", "There was an error, please try again" , "Ok");
        }





    }
    
}