﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using smartcity1.Models;
using System.Collections.ObjectModel;

namespace smartcity1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditDev : ContentPage
    {
        public ObservableCollection<string> Items { get; set; }
        public EditDev()
        {
            InitializeComponent();
            init();
            GetSensorList();
        }

        void init()
        {
            BackgroundColor = Constants.BackgroundColor;
            MyListView.SeparatorColor = Color.White;

        }

        private async void GetSensorList()
        {
            HttpResponseMessage response = await user.client.GetAsync(Constants.BaseUrl + "/smartcity/data.php");
            HttpContent content = response.Content;
            string mycontent = await content.ReadAsStringAsync();
            string[] sensors = mycontent.Split(',');
            indicator.IsVisible = false;
            //indicator.IsEnabled = false;
            indicator.IsRunning = false;
            Items = new ObservableCollection<string>(sensors);
            MyListView.ItemsSource = Items;





        }
        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            await Navigation.PushAsync(new Editor(e.Item.ToString()));

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }



    }
}