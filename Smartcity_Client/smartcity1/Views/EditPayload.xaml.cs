﻿using smartcity1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace smartcity1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditPayload : ContentPage

    
	{

        static string _globalString = "";

        public static string GlobalString
        {
            get
            {
                return _globalString;
            }
            set
            {
                _globalString = value;
            }
        }


        public EditPayload (string SelectedSensor)
		{
			InitializeComponent ();
            EditPayload.GlobalString = SelectedSensor;
            Init();
        }

        void Init()
        {
            BackgroundColor = Constants.BackgroundColor;
            Lbl_Column_Names.TextColor = Constants.MainTextColor;
            Lbl_Format_Size.TextColor = Constants.MainTextColor;
            Btn_Edit.TextColor = Color.Black;
           




            Entry_Column_Names.Completed += (s, e) => Entry_Format_Size.Focus();
            Entry_Format_Size.Completed += (s, e) => UpdateDevicePayload(s, e);
            
        }

        async void UpdateDevicePayload(object sender, EventArgs e)
        {
            string str = EditPayload.GlobalString;
            str = str.Replace(" ", String.Empty);

            var values = new Dictionary<string, string>
            { 
            { "column_names", Entry_Column_Names.Text },
            { "format", Entry_Format_Size.Text },
            { "sensor", str }
            };

            System.Diagnostics.Debug.WriteLine(str);

            var content = new FormUrlEncodedContent(values);
            var response = await user.client.PostAsync(Constants.BaseUrl + "/smartcity/edit_payload.php", content);
            string responseString = await response.Content.ReadAsStringAsync();
            System.Diagnostics.Debug.WriteLine(responseString);



        }
    }
}